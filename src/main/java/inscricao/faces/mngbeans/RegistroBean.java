/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.html.HtmlDataTable;
import utfpr.faces.support.PageBean;

/**
 *
 * @author guilherme
 */
@ManagedBean
@RequestScoped
public class RegistroBean extends PageBean {

    private HtmlDataTable dataTable;
    private List<Candidato> candidatoList = new ArrayList<>();

    public RegistroBean() {
    }
    
    /**
     * Adiciona candidato à lista.
     *
     * @param candidato
     * @return sucesso (true) ou falha (false)
     */
    public boolean addCandidato(Candidato candidato) {
        return candidatoList.add(candidato);
    }

    public HtmlDataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(HtmlDataTable dataTable) {
        this.dataTable = dataTable;
    }

    public List<Candidato> getCandidatoList() {
        return candidatoList;
    }

    public void setCandidatoList(List<Candidato> candidatoList) {
        this.candidatoList = candidatoList;
    }

}
